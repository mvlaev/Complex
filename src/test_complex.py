'''Unit tests for the Complex class.
'''

import unittest
from complex import Complex

class ClassComplexConstructionTestCase(unittest.TestCase):
	def test_constructor_bad_real_part(self):
		'''Constructor tests - Bad real part.
		'''
		with self.assertRaises(ValueError):
			real = 'qwerty'
			img  = 5

			Complex(real=real, img=img)


	def test_constructor_bad_img_part(self):
		'''Constructor tests - Bad img part.
		'''
		with self.assertRaises(ValueError):
			real = 5
			img  = 'qwerty'

			Complex(real=real, img=img)


	def test_missing_input(self):
		'''Constructor tests - Missing parameter.
		'''

		#We don't want default values for real and img, even if they are 0
		with self.assertRaises(TypeError):
			Complex(1)


	def test_constructor_good_input(self):
		'''Constructor tests - Good input.
		'''
		real = 1
		img = 55

		number = Complex(real=real, img=img)
		self.assertEqual(number._Complex__real, real)
		self.assertEqual(number._Complex__img, img)


class ClassComplexGettersTestCase(unittest.TestCase):
	def setUp(self):
		self.real = 1
		self.img = 2
		self.number = Complex(self.real, self.img)

	def test_real_getter(self):
		'''Getters Tests - Real getter.
		'''
		self.assertEqual(self.number.r, self.real)

	def test_img_getter(self):
		'''Getters Tests - Img getter.
		'''
		self.assertEqual(self.number.i, self.img)

class ClassComplexStringReprTestCase(unittest.TestCase):
	def test_all_positive(self):
		'''String representation tests - Real and Img positive.
		'''
		number = Complex(1,2)
		expected = '1.00+2.00i'
		self.assertEqual(number.__str__(), expected)

	def test_all_negative(self):
		'''String representation tests - Real and Img negative.
		'''
		number = Complex(-1,-2)
		expected = '-1.00-2.00i'
		self.assertEqual(number.__str__(), expected)

	def test_zero_element(self):
		'''String representation tests - Real and Img zero.
		'''
		number = Complex(0,0)
		expected = '0.00+0.00i'
		self.assertEqual(number.__str__(), expected)


class ClassComplexOpsTestCase(unittest.TestCase):
	'''Tests the class Complex operations.
	'''
	def setUp(self):
		self.real1 = 5
		self.img1 = 1
		self.number1 = Complex(self.real1, self.img1)

		self.real2 = -1
		self.img2 = 3
		self.number2 = Complex(self.real2, self.img2)

	def test_addition(self):
		'''Operations tests - Addition.
		'''
		expected_real = self.real1 + self.real2
		expected_img = self.img1 + self.img2

		result = self.number1 + self.number2

		self.assertEqual(result.r, expected_real)
		self.assertEqual(result.i, expected_img)

	def test_subtraction(self):
		'''Operations tests - Subtraction.
		'''
		expected_real = self.real1 - self.real2
		expected_img = self.img1 - self.img2

		result = self.number1 - self.number2

		self.assertEqual(result.r, expected_real)
		self.assertEqual(result.i, expected_img)


	def test_multiplication(self):
		'''Operations tests - Multiplication.
		'''
		expected_real = self.real1 * self.real2 - self.img1 * self.img2
		expected_img = self.real1 * self.img2 + self.img1 * self.real2

		result = self.number1 * self.number2

		self.assertEqual(result.r, expected_real)
		self.assertEqual(result.i, expected_img)

	def test_division_non_zero_divisor(self):
		'''Operations tests - Division by non zero.
		'''
		divisor = self.real2 ** 2 + self.img2 ** 2
		expected_real = (self.real1 * self.real2 + self.img1 * self.img2) / divisor
		expected_img = (self.img1 * self.real2 - self.real1 * self.img2) / divisor

		result = self.number1 / self.number2

		self.assertEqual(result.r, expected_real)
		self.assertEqual(result.i, expected_img)

	def test_division_zero_divisor(self):
		'''Operations tests - Division by zero.
		'''
		with self.assertRaises(ZeroDivisionError):
			self.number1 / Complex(0,0)

if __name__ == '__main__':
	unittest.main()
