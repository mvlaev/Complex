class Complex:
	'''Class that represents a complex number.

	The constructor will raise ValueError if the supplied arguments can not be passed to
	float.

	Implemented operations:
	1. Addition of Complex instances.
	2. Subtraction of Complex instances.
	3. Multiplication of Complex instances.
	4. Division of a Complex instance by a non zero one. Raises ZeroDivisionError
	if the divisor has both real and imaginary parts equal to zero.

	The result of the operations is a new instance of Complex.
	'''

	def __init__(self, real: float, img: float):
		'''Convert input values to float and initialize.

		If not possible the method will raise ValueError and will NOT initialize
		the class with wrong type of input.
		'''
		self.__real = float(real)
		self.__img  = float(img)

	@property
	def r(self):
		return self.__real

	@property
	def i(self):
		return self.__img

	def __add__(self, other):
		r = self.r + other.r
		i = self.i + other.i

		return Complex(real = r, img = i)


	def __sub__(self, other):
		r = self.r - other.r
		i = self.i - other.i

		return Complex(real=r, img=i)


	def __mul__(self, other):
		r = self.r * other.r - self.i * other.i
		i = self.r * other.i + self.i * other.r

		return Complex(real=r, img=i)


	def __truediv__(self, other):
		r_pre = self.r * other.r + self.i * other.i
		i_pre = self.i * other.r - self.r * other.i
		d     = other.r * other.r + other.i * other.i

		r = r_pre / d
		i = i_pre / d

		return Complex(real=r, img=i)


	def __str__(self):
		to_return = f'{self.r:.2f}{self.i:+.2f}i'
		return to_return
